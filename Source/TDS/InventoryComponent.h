// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSTypes.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FWeaponDynamicInfo, WeaponDynamicInfo, int, WeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponDynamicInfoChange, int, IndexSlot, FWeaponDynamicInfo, DynamicInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoSlotChange, EWeaponType, TypeAmmo, FAmmoSlot, AmmoSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int, IndexSlotChange, FWeaponSlot, NewInfo);



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();
	
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponDynamicInfoChange OnWeaponDynamicInfoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoSlotChange OnAmmoSlotChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponSlot OnWeaponSlotChange;
	

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TMap<EWeaponType, FAmmoSlot> AmmoSlots;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SwitchToNextWeapon(int CurrentIndex, bool bIsForward);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void SwitchWeaponByIndex_OnServer(int SlotIndex);
	void SetWeaponDynamicInfo(int Index, FWeaponDynamicInfo NewInfo);
	void TakeAmmoFromSlot(EWeaponType WeaponType, int TakeCount);
	
	UFUNCTION(BlueprintCallable)
	int GetAvailableAmmoForWeapon(EWeaponType WeaponType);
	UFUNCTION(BlueprintCallable)
	FWeaponDynamicInfo GetWeaponDynamicInfoByIndex(int Index);
	UFUNCTION(BlueprintCallable)
	FName GetWeaponNameByIndex(int Index);

	//pick up interface
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TryAddAmmoToSlot_OnServer(EWeaponType AmmoType, int AmmoToAdd, AActor* PickupActor);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TrySaveWeaponToFreeSlot_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon, ATDSCharacter* Character);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void ReplaceWeapon_OnServer(FWeaponSlot NewWeapon, int SlotIndex, AActor* PickUpActor);
	

private:
	UFUNCTION(Server, Reliable)
	void InitInventory_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void OnAmmoSlotChange_Multicast(EWeaponType WeaponType, FAmmoSlot AmmoSlot);

	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponSwitch_Multicast(FName WeaponId, FWeaponDynamicInfo WeaponDynamicInfo, int Index);

	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponDynamicInfoChange_Multicast(int Index, FWeaponDynamicInfo WeaponDynamicInfo);

	UFUNCTION(NetMulticast, Reliable)
	void OnUpdateWeaponSlot_Multicast(int Index, FWeaponSlot WeaponSlot);
	

	int32 MaxSlotsWeapon = 0;

	

		
};
