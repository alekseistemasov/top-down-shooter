// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "ProjectileBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "CharHealthComponent.h"
#include "Net/UnrealNetwork.h"


// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetGenerateOverlapEvents(false);
	SkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMesh->SetupAttachment(RootComponent);
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMesh->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ShootLocation->SetupAttachment(RootComponent);

	MagazineDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MagazineDropLocation"));
	MagazineDropLocation->SetupAttachment(RootComponent);

	BulletShellLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("BulletShellDropLocation"));
	BulletShellLocation->SetupAttachment(RootComponent);

	SetReplicates(true);



}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	
	
}

void AWeaponBase::InitWeapon(const FWeaponInfo& WeaponInfo, const FWeaponDynamicInfo& DynamicInfo, int WeaponIndex)
{
	if (SkeletalMesh && !SkeletalMesh->MeshObject)
		SkeletalMesh->DestroyComponent();
	if (StaticMesh && !StaticMesh->GetStaticMesh().Get())
		StaticMesh->DestroyComponent();

	WeaponSetting = WeaponInfo;
	WeaponDynamicInfo = DynamicInfo;
	CurrentWeaponIndex = WeaponIndex;
	
}

void AWeaponBase::SetWeaponFiringState_OnServer_Implementation(bool bIsFiring)
{
	if (!bIsFireBlock)
		bWeaponIsFiring = bIsFiring;
	else
		bWeaponIsFiring = false;
}

void AWeaponBase::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		if (bWeaponIsFiring)
			if (FireTimer <= 0)
			{
				if (!bWeaponIsReloading)
					Fire();
			}
			
	}
	/*else
	{
		if (!bWeaponIsReloading && FireTimer <= 0)
		{
			InitReload();
		}
	}*/

	if (FireTimer > 0)
		FireTimer -= DeltaTime;
}

void AWeaponBase::DispersionTick(float DeltaTime)
{
	if (!bWeaponIsReloading)
	{
		if (!bWeaponIsFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion -= CurrentDispersionReduction;
			else
				CurrentDispersion += CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponBase::ReloadTick(float DeltaTime)
{
	if (bWeaponIsReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReloadAnim();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::DropMagazineTick(float DeltaTime)
{
	if (bDropMagazineFlag)
		if (DropMagazineTimer <= 0) {
			InitDropMesh_OnServer(WeaponSetting.MagazineDropInfo.MeshClass, MagazineDropLocation->GetComponentLocation(), MagazineDropLocation->GetComponentRotation(),
				WeaponSetting.MagazineDropInfo.LifeSpan, WeaponSetting.MagazineDropInfo.ImpulseDir, WeaponSetting.MagazineDropInfo.DirDisperion,
				WeaponSetting.MagazineDropInfo.ImpulsePower, WeaponSetting.MagazineDropInfo.CustomMass);
			bDropMagazineFlag = false;
		}
		else
			DropMagazineTimer -= DeltaTime;
}

void AWeaponBase::DropShellTick(float DeltaTime)
{
	if (bDropShellFlag)
		if (DropShellTimer <= 0) {
			InitDropMesh_OnServer(WeaponSetting.ShellBulletsInfo.MeshClass, BulletShellLocation->GetComponentLocation(),
				BulletShellLocation->GetComponentRotation(), WeaponSetting.ShellBulletsInfo.LifeSpan,
				BulletShellLocation->GetForwardVector(), WeaponSetting.ShellBulletsInfo.DirDisperion,
				WeaponSetting.ShellBulletsInfo.ImpulsePower, WeaponSetting.ShellBulletsInfo.CustomMass);
			bDropShellFlag = false;
		}
		else
			DropShellTimer -= DeltaTime;
}

int AWeaponBase::GetWeaponRound()
{
	return WeaponDynamicInfo.Round;
}

FWeaponDynamicInfo AWeaponBase::GetWeaponDynamicInfo()
{
	return WeaponDynamicInfo;
}

bool AWeaponBase::IsWeaponReloading()
{
	return bWeaponIsReloading;
}

void AWeaponBase::UpdateWeaponByCharacterMovement_OnServer_Implementation(FVector NewShootEndLocation, bool bShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = bShouldReduceDispersion;
}

void AWeaponBase::Fire()
{
	// On Server
	FireTimer = WeaponSetting.RateOfFire;
	WeaponDynamicInfo.Round--;
	
	OnWeaponFire.Broadcast(WeaponSetting.AnimCharFire, WeaponSetting.AnimCharFireAim);
	
	ChangeDispersionByShot();

	if (WeaponSetting.AnimWeaponFire)
	{
		PlayWeaponAnim_Multicast(WeaponSetting.AnimWeaponFire);
	}
	
	if (WeaponSetting.ShellBulletsInfo.MeshClass) {
		bDropShellFlag = true;
		DropShellTimer = WeaponSetting.ShellBulletsInfo.DropTime;
	}

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileBase* myProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				//Projectile null Init trace fire	
				
				FHitResult hitResult;
				FCollisionQueryParams params;
				params.bReturnPhysicalMaterial = true;
				UCharHealthComponent* healthComp = nullptr;
				
                GetWorld()->LineTraceSingleByChannel(hitResult, SpawnLocation, EndLocation, ECC_EngineTraceChannel4, params);
				EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(hitResult);
				if (hitResult.GetActor())
				{
					healthComp = hitResult.GetActor()->GetComponentByClass<UCharHealthComponent>();
				}
				
				if (healthComp && healthComp->GetShieldValue() > 0)
				{
					mySurfacetype = EPhysicalSurface::SurfaceType3;
				}
				
				if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
				{
					UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

					if (myMaterial && hitResult.GetComponent())
					{
						SpawnHitDecal_Multicast(myMaterial, hitResult.GetComponent(), hitResult);
					}
				}
				if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
				{
					UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
					if (myParticle)
					{
						SpawnHitFX_Multicast(myParticle, hitResult);
					}
				}

				if (WeaponSetting.ProjectileSetting.HitSound)
				{
					SpawnHitSound_Multicast(WeaponSetting.ProjectileSetting.HitSound, hitResult);
				}

			
			    UGameplayStatics::ApplyDamage(hitResult.GetActor(), WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);
				UTDSTypes::AddEffectBySurfaceType(hitResult.GetActor(), WeaponSetting.ProjectileSetting.Effect, mySurfacetype, hitResult.BoneName);
				
			}
		}
	}
}

FProjectileInfo AWeaponBase::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

int AWeaponBase::GetNumberProjectileByShot()
{
	return WeaponSetting.NumberProjectileByShot;
}

FVector AWeaponBase::GetFireEndLocation()
{
	ShootEndLocation.Z = ShootLocation->GetComponentLocation().Z;
	FVector EndLocation = FVector();
	
    FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}


	return EndLocation;
}

FVector AWeaponBase::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

float AWeaponBase::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

void AWeaponBase::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

void AWeaponBase::InitReloadAnim()
{
	//On Server
	bWeaponIsReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;
	//RoundAfterReloading = NewRoundAfterReloading;

	
	if (SkeletalMesh && SkeletalMesh->GetAnimInstance() && WeaponSetting.AnimWeaponReload) {
		float playRate = WeaponSetting.AnimWeaponReload->GetPlayLength() / WeaponSetting.ReloadTime;
		PlayWeaponAnim_Multicast(WeaponSetting.AnimWeaponReload, playRate);
	}

	if (WeaponSetting.AnimCharReload && WeaponSetting.AnimCharReloadAim)
		OnWeaponReloadAnimStart_Multicast(WeaponSetting.AnimCharReload, WeaponSetting.AnimCharReloadAim, WeaponSetting.ReloadTime);

	if (WeaponSetting.MagazineDropInfo.MeshClass) {
		bDropMagazineFlag = true;
		DropMagazineTimer = WeaponSetting.MagazineDropInfo.DropTime;
	}
		
}

void AWeaponBase::Reload(int AvailableAmmo, int& AmmoTaken)
{
	int ammoNeeded = WeaponSetting.MaxRound - WeaponDynamicInfo.Round;
	if (AvailableAmmo > ammoNeeded)
		AmmoTaken = ammoNeeded;
	else
		AmmoTaken = AvailableAmmo;
	
	WeaponDynamicInfo.Round += AmmoTaken;
}

void AWeaponBase::CancelReload()
{
	bWeaponIsReloading = false;
	if (SkeletalMesh && SkeletalMesh->GetAnimInstance())
		SkeletalMesh->GetAnimInstance()->StopAllMontages(0.15f);
	OnWeaponReloadAnimEnd.Broadcast(false);
	bDropMagazineFlag = false;
}

void AWeaponBase::FinishReloadAnim()
{
	bWeaponIsReloading = false;
	/*int ammoTaken = RoundAfterReloading - WeaponDynamicInfo.Round;
	WeaponDynamicInfo.Round = RoundAfterReloading;*/
	OnWeaponReloadAnimEnd.Broadcast(true);
}

void AWeaponBase::SpawnHitSound_Multicast_Implementation(USoundBase* Sound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, HitResult.ImpactPoint);
}

void AWeaponBase::SpawnHitFX_Multicast_Implementation(UParticleSystem* Template, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Template, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeaponBase::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AWeaponBase::SpawnShellMesh_Multicast_Implementation(TSubclassOf<AStaticMeshActor> MeshClass, FVector SpawnLocation, 
	FRotator SpawnRotation, float LifeSpan, FVector ImpulseDir, float ImpulseDispersion, float ImpulsePower, float CustomMass)
{
	AStaticMeshActor* newActor;
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	newActor = GetWorld()->SpawnActor<AStaticMeshActor>(MeshClass.Get(), SpawnLocation, SpawnRotation, SpawnParams);
	if (newActor && newActor->GetStaticMeshComponent()) {
		newActor->SetActorTickEnabled(false);
		newActor->SetLifeSpan(LifeSpan);
		newActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		newActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		newActor->GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
		newActor->GetStaticMeshComponent()->SetSimulatePhysics(true);


		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
		newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (CustomMass > 0)
			newActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass);

		if (!ImpulseDir.IsNearlyZero()) {
			FVector finalDir = ImpulseDir;
			if (!FMath::IsNearlyZero(ImpulseDispersion))
				finalDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(ImpulseDir, ImpulseDispersion);
			newActor->GetStaticMeshComponent()->AddImpulse(finalDir.GetSafeNormal() * ImpulsePower);
		}
	}
}

void AWeaponBase::InitDropMesh_OnServer_Implementation(TSubclassOf<AStaticMeshActor> MeshClass, FVector SpawnLocation, FRotator SpawnRotation, float LifeSpan, 
	FVector ImpulseDir, float ImpulseDispersion, float ImpulsePower, float CustomMass)
{
	SpawnShellMesh_Multicast(MeshClass, SpawnLocation, SpawnRotation, LifeSpan, ImpulseDir, ImpulseDispersion, ImpulsePower, CustomMass);

}

void AWeaponBase::PlayWeaponAnim_Multicast_Implementation(UAnimMontage* Anim, float PlayRate = 1)
{
	if (SkeletalMesh && SkeletalMesh->GetAnimInstance() && Anim)
		SkeletalMesh->GetAnimInstance()->Montage_Play(Anim, PlayRate);
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		DispersionTick(DeltaTime);
		ReloadTick(DeltaTime);
		DropMagazineTick(DeltaTime);
		DropShellTick(DeltaTime);
	}

}

void AWeaponBase::UpdateMovementState_OnServer_Implementation(EMovementState State)
{
	switch (State)
	{
	case EMovementState::AIM:
		bIsFireBlock = false;
		ShouldReduceDispersion = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::WALK_AIM:
		bIsFireBlock = false;
		ShouldReduceDispersion = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::WALK:
		bIsFireBlock = false;
		ShouldReduceDispersion = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::RUN:
		bIsFireBlock = false;
		ShouldReduceDispersion = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::RUN_SPRINT:
		bIsFireBlock = true;
		SetWeaponFiringState_OnServer(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}

void AWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponBase, WeaponDynamicInfo);
}

void AWeaponBase::OnWeaponReloadAnimStart_Multicast_Implementation(UAnimMontage* ReloadAnim, UAnimMontage* ReloadAnimAim, float ReloadTime)
{
	OnWeaponReloadAnimStart.Broadcast(ReloadAnim, ReloadAnimAim, ReloadTime);
}
