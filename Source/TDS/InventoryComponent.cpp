// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "Net/UnrealNetwork.h"
#include "TDSCharacter.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	MaxSlotsWeapon = WeaponSlots.Num();

	InitInventory_OnServer();

	
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponent::SwitchToNextWeapon(int CurrentIndex, bool bIsForward)
{
	//On Server
	bool bNotEmptySlotFound = false;
	int Index = CurrentIndex;
	while (!bNotEmptySlotFound) {
		if (bIsForward)
			Index++;
		else
			Index--;
		
		if (Index > WeaponSlots.Num() - 1)
			Index = 0;
		if (Index < 0)
			Index = WeaponSlots.Num() - 1;

        if (WeaponSlots.IsValidIndex(Index) && !WeaponSlots[Index].NameItem.IsNone()) {
			OnWeaponSwitch_Multicast(WeaponSlots[Index].NameItem, WeaponSlots[Index].DynamicInfo, Index);
			bNotEmptySlotFound = true;
		}
		
	}
}

void UInventoryComponent::SwitchWeaponByIndex_OnServer_Implementation(int SlotIndex)
{
	if (WeaponSlots.IsValidIndex(SlotIndex) && !WeaponSlots[SlotIndex].NameItem.IsNone()) {
		//OnSwitchWeapon.Broadcast(WeaponSlots[SlotIndex].NameItem, WeaponSlots[SlotIndex].DynamicInfo, SlotIndex);
		OnWeaponSwitch_Multicast(WeaponSlots[SlotIndex].NameItem, WeaponSlots[SlotIndex].DynamicInfo, SlotIndex);
	}
}

void UInventoryComponent::SetWeaponDynamicInfo(int Index, FWeaponDynamicInfo NewInfo)
{
	//On Server
	if (WeaponSlots.IsValidIndex(Index)) {
		WeaponSlots[Index].DynamicInfo = NewInfo;
		OnWeaponDynamicInfoChange_Multicast(Index, WeaponSlots[Index].DynamicInfo);
	}
}

void UInventoryComponent::TakeAmmoFromSlot(EWeaponType WeaponType, int TakeCount)
{
	//On Server
	if (AmmoSlots.Contains(WeaponType)) {
		AmmoSlots[WeaponType].Count -= TakeCount;
		OnAmmoSlotChange_Multicast(WeaponType, AmmoSlots[WeaponType]);
	}
}



int UInventoryComponent::GetAvailableAmmoForWeapon(EWeaponType WeaponType)
{
	if (AmmoSlots.Contains(WeaponType))
		return AmmoSlots[WeaponType].Count;
	else
		return 0;
}

FWeaponDynamicInfo UInventoryComponent::GetWeaponDynamicInfoByIndex(int Index)
{
	if (WeaponSlots.IsValidIndex(Index))
		return WeaponSlots[Index].DynamicInfo;
	else
		return FWeaponDynamicInfo();
}

FName UInventoryComponent::GetWeaponNameByIndex(int Index)
{
	if (WeaponSlots.IsValidIndex(Index))
		return WeaponSlots[Index].NameItem;
	else
		return FName();
}



void UInventoryComponent::TryAddAmmoToSlot_OnServer_Implementation(EWeaponType AmmoType, int AmmoToAdd, AActor* PickUpActor)
{
	if (AmmoSlots.Contains(AmmoType)) {
		if (AmmoSlots[AmmoType].Count >= AmmoSlots[AmmoType].MaxCount)
			return;
		
		AmmoSlots[AmmoType].Count += AmmoToAdd;
		if (AmmoSlots[AmmoType].Count > AmmoSlots[AmmoType].MaxCount)
			AmmoSlots[AmmoType].Count = AmmoSlots[AmmoType].MaxCount;
		OnAmmoSlotChange_Multicast(AmmoType, AmmoSlots[AmmoType]);
		PickUpActor->Destroy();
	}
	
	
}

void UInventoryComponent::TrySaveWeaponToFreeSlot_OnServer_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon, ATDSCharacter* Character)
{
	for (int i = 0; i < WeaponSlots.Num(); i++) {
		if (WeaponSlots[i].NameItem.IsNone()) {
			WeaponSlots[i] = NewWeapon;
			OnUpdateWeaponSlot_Multicast(i, NewWeapon);
			PickUpActor->Destroy();
			return;
			
		}
	}
	Character->OnSaveWeaponToFreeSlotFail_OnClient(PickUpActor);
	
}

void UInventoryComponent::ReplaceWeapon_OnServer_Implementation(FWeaponSlot NewWeapon, int SlotIndex, AActor* PickUpActor)
{
	if (WeaponSlots.IsValidIndex(SlotIndex)) {
		FWeaponSlot oldWeapon = WeaponSlots[SlotIndex];
		WeaponSlots[SlotIndex] = NewWeapon;
		OnUpdateWeaponSlot_Multicast(SlotIndex, WeaponSlots[SlotIndex]);
		PickUpActor->Destroy();
		ATDSCharacter* pawn = Cast<ATDSCharacter>(GetOwner());
		if (pawn)
		{
			pawn->InitDropWeapon(oldWeapon);
		}
		//return oldWeapon;
	}
	//return FWeaponSlot();
}

void UInventoryComponent::OnUpdateWeaponSlot_Multicast_Implementation(int Index, FWeaponSlot WeaponSlot)
{
	OnWeaponSlotChange.Broadcast(Index, WeaponSlot);
}

void UInventoryComponent::OnWeaponDynamicInfoChange_Multicast_Implementation(int Index, FWeaponDynamicInfo WeaponDynamicInfo)
{
	OnWeaponDynamicInfoChange.Broadcast(Index, WeaponDynamicInfo);
}

void UInventoryComponent::OnWeaponSwitch_Multicast_Implementation(FName WeaponId, FWeaponDynamicInfo WeaponDynamicInfo, int Index)
{
	OnSwitchWeapon.Broadcast(WeaponId, WeaponDynamicInfo, Index);
}

void UInventoryComponent::OnAmmoSlotChange_Multicast_Implementation(EWeaponType WeaponType, FAmmoSlot AmmoSlot)
{
	OnAmmoSlotChange.Broadcast(WeaponType, AmmoSlot);
}

void UInventoryComponent::InitInventory_OnServer_Implementation()
{
	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].DynamicInfo, 0);
	}

}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, WeaponSlots);
	

}

