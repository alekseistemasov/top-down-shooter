// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IGameInteractable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIGameInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API IIGameInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual TArray<class UTDSStateEffect*> GetAllCurrentEffects() = 0;
	virtual void RemoveEffect(UTDSStateEffect* RemoveEffect) = 0;
	virtual void AddEffect(UTDSStateEffect* newEffect) = 0;
	//virtual FName GetAttachBoneName() = 0;
	virtual FVector GetAttachLocation() = 0;
};
