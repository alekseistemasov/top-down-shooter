// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDSTypes.h"
#include "IGameInteractable.h"
#include "TDSCharacter.generated.h"

class AWeaponBase;




UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public IIGameInteractable
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComp) override;

	virtual void BeginPlay() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UCharHealthComponent* CharHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere)
	FCharacterSpeed SpeedInfo;

	UPROPERTY()
	EMovementState MovementState = EMovementState::RUN;

	UPROPERTY(EditAnywhere)
	float SprintAcelerationRate = 100;

	UPROPERTY(EditAnywhere, Category = "Movement")
	TArray<UAnimMontage*> DeathAnims;

	UPROPERTY(Replicated)
	TArray<UTDSStateEffect*> StateEffects;
	UPROPERTY(ReplicatedUsing = EffectAdded_OnRep)
	UTDSStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemoved_OnRep)
	UTDSStateEffect* EffectRemove = nullptr;
	
	TArray<UParticleSystemComponent*> ParticleComponents;
	
	FTimerHandle DeathAnimEndTimer;

	float AxisX = 0;
	float AxisY = 0;

	
	

	
	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAttackPressed();

	UFUNCTION()
	void InputAttackReleased();

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION()
	void WeaponReloadAnimStart(UAnimMontage* Anim, UAnimMontage* AnimAim, float ReloadTime);

	UFUNCTION()
	void WeaponReloadAnimEnd(bool bIsSuccess);

	UFUNCTION()
	void OnWeaponFire(UAnimMontage* AnimFire, UAnimMontage* AnimFireAim);

	UFUNCTION()
	void OnDead();

	UFUNCTION()
	void OnShieldChange(float ShieldValue);

	UFUNCTION(Server, Unreliable)
	void SetCharYaw_OnServer(float Yaw);
	
	UFUNCTION(Server, Reliable)
	void ChangeMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void ChangeMovementState_Multicast(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_Multicast(UAnimMontage* Anim, bool bPauseAfterAnim);

	UFUNCTION()
	void EffectAdded_OnRep();
	UFUNCTION()
	void EffectRemoved_OnRep();
	
	void SwitchEffect(UTDSStateEffect* Effect, bool bIsAdd);

	UFUNCTION(NetMulticast, Reliable)
	void AddEffectFX_Multicast(UParticleSystem* Template, FName BoneName);

	//Inventory Func
	UFUNCTION(Server, Reliable)
	void TrySwicthNextWeapon_OnServer();
	UFUNCTION(Server, Reliable)
	void TrySwitchPreviosWeapon_OnServer();

	
    void MovementTick(float DeltaSeconds);

	void CharacterUpdate();

	void SprintAceleration(float DeltaSeconds);

	UFUNCTION(NetMulticast, Reliable)
	void DisableCollision_Multicast();
	UFUNCTION()
	void PauseAllAnims();

	EPhysicalSurface GetSurfaceType();

protected:
	UPROPERTY(BlueprintReadWrite)
	bool AimEnabled = false;

	UPROPERTY(BlueprintReadWrite)
	bool WalkEnabled = false;

	UPROPERTY(BlueprintReadWrite)
	bool SprintEnabled = false;

	UPROPERTY(EditAnywhere, Category = "Init")
	FName InitWeaponName = "";

	UPROPERTY(Replicated)
	AWeaponBase* CurrentWeapon = nullptr;

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim, UAnimMontage* AnimAim, float ReloadTime);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP();

	UFUNCTION(BlueprintNativeEvent)
	void OnWeaponFire_BP(UAnimMontage* AnimFire, UAnimMontage* AnimFireAim);

	UFUNCTION(BlueprintNativeEvent)
	void OnWeaponChanged_BP(bool bIsPistol);

	UFUNCTION()
	void InitWeapon(FName WeaponId, FWeaponDynamicInfo DynamicInfo, int WeaponIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void ActivatePickUpChecking_BP();

public:
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(BlueprintCallable)
	AWeaponBase* GetCurrentWeapon();
	UFUNCTION(Client, Reliable)
	void OnSaveWeaponToFreeSlotFail_OnClient(AActor* PickUpActor);

	UFUNCTION(BlueprintImplementableEvent)
	void InitDropWeapon(FWeaponSlot DropInfo);

	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	//interface
	TArray<class UTDSStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDSStateEffect* RemoveEffect) override;
	void AddEffect(UTDSStateEffect* newEffect) override;
	//FName GetAttachBoneName() override;
	FVector GetAttachLocation() override;


	
};

