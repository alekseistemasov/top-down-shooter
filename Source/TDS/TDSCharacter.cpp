// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDSGameInstance.h"
#include "WeaponBase.h"
#include "InventoryComponent.h"
#include "CharHealthComponent.h"
#include "TDSPlayerController.h"
#include "Engine/DamageEvents.h"
#include "ProjectileBase.h"
#include "Net/UnrealNetwork.h"
#include "TDSStateEffect.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"


ATDSCharacter::ATDSCharacter()
{
	CharacterUpdate();
	SetReplicates(true);
	
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UCharHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent && HasAuthority())
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::OnDead);
		CharHealthComponent->OnShieldChange.AddDynamic(this, &ATDSCharacter::OnShieldChange);
	}

	if (InventoryComponent && HasAuthority())
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);

	if (MovementState == EMovementState::RUN_SPRINT 
		&& GetCharacterMovement()->MaxWalkSpeed < SpeedInfo.RunSprintSpeed) {
			SprintAceleration(DeltaSeconds);
		}
		
		
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	InputComp->BindAxis("MoveForward", this, &ATDSCharacter::InputAxisX);
	InputComp->BindAxis("MoveRight", this, &ATDSCharacter::InputAxisY);

	InputComp->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	InputComp->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	InputComp->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TryReloadWeapon_OnServer);

	InputComp->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwicthNextWeapon_OnServer);
	InputComp->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviosWeapon_OnServer);

}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	if (!(CharHealthComponent && CharHealthComponent->GetIsAlive())) return;
	
	if (CurrentWeapon) {
		if (CurrentWeapon->GetWeaponRound() <= 0) {
			TryReloadWeapon_OnServer();
			CurrentWeapon->SetWeaponFiringState_OnServer(true);
		}
		else
            CurrentWeapon->SetWeaponFiringState_OnServer(true);
	}
}

void ATDSCharacter::InputAttackReleased()
{
	if (CurrentWeapon)
		CurrentWeapon->SetWeaponFiringState_OnServer(false);
}

void ATDSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon)
	{
		EWeaponType CurrentWeaponType = CurrentWeapon->WeaponSetting.WeaponType;
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && !CurrentWeapon->IsWeaponReloading()
			&& InventoryComponent->GetAvailableAmmoForWeapon(CurrentWeaponType) > 0) {
			
			CurrentWeapon->InitReloadAnim();
		}
	}
}

void ATDSCharacter::WeaponReloadAnimStart(UAnimMontage* Anim, UAnimMontage* AnimAim, float ReloadTime)
{
	WeaponReloadStart_BP(Anim, AnimAim, ReloadTime);
}

void ATDSCharacter::WeaponReloadAnimEnd(bool bIsSuccess)
{
	if (bIsSuccess && CurrentWeapon) {
		int ammoTaken = 0;
		CurrentWeapon->Reload(InventoryComponent->GetAvailableAmmoForWeapon(CurrentWeapon->WeaponSetting.WeaponType), ammoTaken);
		InventoryComponent->TakeAmmoFromSlot(CurrentWeapon->WeaponSetting.WeaponType, ammoTaken);
		InventoryComponent->SetWeaponDynamicInfo(CurrentWeapon->CurrentWeaponIndex, CurrentWeapon->GetWeaponDynamicInfo());
	}
	WeaponReloadEnd_BP();
}

void ATDSCharacter::OnWeaponFire(UAnimMontage* AnimFire, UAnimMontage* AnimFireAim)
{
	if (CurrentWeapon) {
		InventoryComponent->SetWeaponDynamicInfo(CurrentWeapon->CurrentWeaponIndex, CurrentWeapon->GetWeaponDynamicInfo());
		
	}
	OnWeaponFire_BP(AnimFire, AnimFireAim);
}

void ATDSCharacter::OnDead()
{
	//On Server
	InputAttackReleased();
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeathAnims.Num());
	if (DeathAnims.IsValidIndex(rnd))
	{
		PlayAnim_Multicast(DeathAnims[rnd], true);
	}

	DisableCollision_Multicast();
	
	
	
}

void ATDSCharacter::OnShieldChange(float ShieldValue)
{
    
	
}

void ATDSCharacter::ChangeMovementState_OnServer_Implementation(EMovementState NewState)
{
	ChangeMovementState_Multicast(NewState);
}



void ATDSCharacter::SetCharYaw_OnServer_Implementation(float Yaw)
{
	if (GetController() && !GetController()->IsLocalController())
	{
		SetActorRotation(FQuat(FRotator(0, Yaw, 0)));
	}
}

void ATDSCharacter::ChangeMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	if (NewState == EMovementState::AIM || NewState == EMovementState::WALK_AIM)
	{
		AimEnabled = true;
	}
	CharacterUpdate();
}

void ATDSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim, bool bPauseAfterAnim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance() && Anim)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
		GetWorldTimerManager().SetTimer(DeathAnimEndTimer, this, &ATDSCharacter::PauseAllAnims, Anim->GetPlayLength() - 0.1, false);
	}
}

void ATDSCharacter::TrySwicthNextWeapon_OnServer_Implementation()
{
	if (InventoryComponent && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		if (CurrentWeapon)
		{
			if (CurrentWeapon->IsWeaponReloading())
				CurrentWeapon->CancelReload();
			InventoryComponent->SwitchToNextWeapon(CurrentWeapon->CurrentWeaponIndex, true);
		}

		
	}
}

void ATDSCharacter::TrySwitchPreviosWeapon_OnServer_Implementation()
{
	if (InventoryComponent && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
        if (CurrentWeapon)
		{
			if (CurrentWeapon->IsWeaponReloading())
				CurrentWeapon->CancelReload();
			InventoryComponent->SwitchToNextWeapon(CurrentWeapon->CurrentWeaponIndex, false);
		}


	}
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim, UAnimMontage* AnimAim, float ReloadTime)
{
	// in BP
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void ATDSCharacter::OnWeaponFire_BP_Implementation(UAnimMontage* AnimFire, UAnimMontage* AnimFireAim) 
{
	// in BP
}

void ATDSCharacter::OnWeaponChanged_BP_Implementation(bool bIsPistol)
{
	// in BP
}

void ATDSCharacter::MovementTick(float DeltaSeconds)
{
	if (!(CharHealthComponent && CharHealthComponent->GetIsAlive()) || !Cast<ATDSPlayerController>(GetController())) return;
	if (!(GetController() && GetController()->IsLocalPlayerController())) return;
	
	AddMovementInput(FVector::ForwardVector, AxisY);
	AddMovementInput(FVector::RightVector, -AxisX);

	APlayerController* controller = UGameplayStatics::GetPlayerController(this, 0);
	if (controller) {
		FHitResult hitResult;
		controller->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery3, false, hitResult);
		FRotator rotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), hitResult.Location);
		rotator.Pitch = 0;
		rotator.Roll = 0;
		SetActorRotation(FQuat::MakeFromRotator(rotator));
		SetCharYaw_OnServer(rotator.Yaw);

		if (CurrentWeapon)
		{
			//CurrentWeapon->ShootEndLocation = hitResult.Location;
			CurrentWeapon->UpdateWeaponByCharacterMovement_OnServer(hitResult.Location, true);
		}

	}

}

void ATDSCharacter::CharacterUpdate()
{
	float speed = 0;
	switch (MovementState) {
	case EMovementState::AIM:
		speed = SpeedInfo.AimSpeed;
		break;
	case EMovementState::WALK:
		speed = SpeedInfo.WalkSpeed;
		break;
	case EMovementState::WALK_AIM:
		speed = SpeedInfo.WalkAimSpeed;
		break;
	case EMovementState::RUN:
		speed = SpeedInfo.RunSpeed;
		break;
	case EMovementState::RUN_SPRINT:
		speed = GetCharacterMovement()->MaxWalkSpeed;
		break;
	default:
		break;
		
	}
	GetCharacterMovement()->MaxWalkSpeed = speed;
	if (CurrentWeapon)
	{
		CurrentWeapon->UpdateMovementState_OnServer(MovementState);
	}
}

void ATDSCharacter::SprintAceleration(float DeltaSeconds)
{
	GetCharacterMovement()->MaxWalkSpeed += SprintAcelerationRate * DeltaSeconds;
}

void ATDSCharacter::DisableCollision_Multicast_Implementation()
{
	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}

	
}





void ATDSCharacter::PauseAllAnims()
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->bPauseAnims = true;
	}
}

EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
	if (CharHealthComponent->GetShieldValue() > 0)
		return EPhysicalSurface::SurfaceType3;
	
	if (GetMesh() && GetMesh()->GetMaterial(0) && GetMesh()->GetMaterial(0)->GetPhysicalMaterial())
	    return GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;
	
	return EPhysicalSurface::SurfaceType1;
}

void ATDSCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::RUN;
	if (SprintEnabled) {
		NewState = EMovementState::RUN_SPRINT;
		
	}
	else if (AimEnabled && WalkEnabled) {
		NewState = EMovementState::WALK_AIM;
		
	}
	else if (AimEnabled) {
		NewState = EMovementState::AIM;
	}
	else if (WalkEnabled) {
		NewState = EMovementState::WALK;
	}
	else {
		NewState = EMovementState::RUN;
	}
	
	ChangeMovementState_OnServer(NewState);

	
}

void ATDSCharacter::InitWeapon(FName WeaponId, FWeaponDynamicInfo DynamicInfo, int WeaponIndex)
{
	//On Server
	if (!HasAuthority()) return;
	
	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UTDSGameInstance* Gi = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo weaponInfo;
	if (Gi) {
	    Gi->GetWeaponInfoByName(WeaponId, weaponInfo);
	}

	if (weaponInfo.WeaponClass) {
		FVector spawnLocation = FVector();
		FRotator spawnRotation = FRotator();

		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		spawnParams.Owner = GetOwner();
		spawnParams.Instigator = GetInstigator();

		AWeaponBase* newWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor<AWeaponBase>(weaponInfo.WeaponClass, spawnParams));
		if (newWeapon) {
			FAttachmentTransformRules rule(EAttachmentRule::SnapToTarget, false);
			newWeapon->AttachToComponent(GetMesh(), rule, FName("hand_rSocket"));
			CurrentWeapon = newWeapon;
			CurrentWeapon->InitWeapon(weaponInfo, DynamicInfo, WeaponIndex);
			CurrentWeapon->UpdateMovementState_OnServer(MovementState);
			CurrentWeapon->OnWeaponReloadAnimStart.AddDynamic(this, &ATDSCharacter::WeaponReloadAnimStart);
			CurrentWeapon->OnWeaponReloadAnimEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadAnimEnd);
			CurrentWeapon->OnWeaponFire.AddDynamic(this, &ATDSCharacter::OnWeaponFire);
			OnWeaponChanged_BP(WeaponId == "Pistol");
		}

	}
}

float ATDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	
	CharHealthComponent->ReceiveDamage_OnServer(DamageAmount);

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileBase* myProjectile = Cast<AProjectileBase>(DamageCauser);
		if (myProjectile)
		{
				UTDSTypes::AddEffectBySurfaceType(this, myProjectile->GetProjectileSetting().Effect, GetSurfaceType(), NAME_None);
		}
	}
		
	

	return ActualDamage;
}

AWeaponBase* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::OnSaveWeaponToFreeSlotFail_OnClient_Implementation(AActor* PickUpActor)
{
	ActivatePickUpChecking_BP();
}

TArray<class UTDSStateEffect*> ATDSCharacter::GetAllCurrentEffects()
{
	return StateEffects;
}

void ATDSCharacter::RemoveEffect(UTDSStateEffect* RemoveEffect)
{
	//On Server
	StateEffects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroy)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATDSCharacter::AddEffect(UTDSStateEffect* newEffect)
{
	//On Server
	StateEffects.Add(newEffect);

	if (!newEffect->bIsAutoDestroy)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		AddEffectFX_Multicast(newEffect->ParticleEffect, newEffect->BoneName);
	}
}



FVector ATDSCharacter::GetAttachLocation()
{
	return FVector();
}

void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSCharacter, CurrentWeapon);
	DOREPLIFETIME(ATDSCharacter, StateEffects);
	DOREPLIFETIME(ATDSCharacter, EffectAdd);
	DOREPLIFETIME(ATDSCharacter, EffectRemove);
}

void ATDSCharacter::EffectAdded_OnRep()
{
	SwitchEffect(EffectAdd, true);
}

void ATDSCharacter::EffectRemoved_OnRep()
{
	SwitchEffect(EffectRemove, false);
}

void ATDSCharacter::SwitchEffect(UTDSStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName HitBoneName = Effect->BoneName;
			USkeletalMeshComponent* skeletal = GetMesh();
			
			if (skeletal && HitBoneName != NAME_None)
			{
				UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, skeletal, HitBoneName, FVector(),
					FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleComponents.Add(particleComponent);
			}
			else
			{
				UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, GetRootComponent(), HitBoneName, FVector(),
					FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleComponents.Add(particleComponent);
			}
		}
	}
	else
	{
		int removingParticleIndex = ParticleComponents.IndexOfByPredicate([=](UParticleSystemComponent* Component)
			{return Effect && Effect->ParticleEffect && Component && Component->Template && Effect->ParticleEffect == Component->Template; });
		
		if (ParticleComponents.IsValidIndex(removingParticleIndex))
		{
			ParticleComponents[removingParticleIndex]->DeactivateSystem();
			ParticleComponents[removingParticleIndex]->DestroyComponent();
			ParticleComponents.RemoveAt(removingParticleIndex);
		}
		
	}
}

void ATDSCharacter::AddEffectFX_Multicast_Implementation(UParticleSystem* Template, FName BoneName)
{
	UTDSTypes::AddEffectFX(Template, this, BoneName, FVector());
}


bool ATDSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	
	for (UTDSStateEffect* effect : StateEffects)
	{
		if (effect)
		{
			WroteSomething |= Channel->ReplicateSubobject(effect, *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)
		}
	}

	return WroteSomething;

	
					
	
}
