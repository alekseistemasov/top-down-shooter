// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnemyBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "TDSStateEffect.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "TDSTypes.h"

// Sets default values
ATDSEnemyBase::ATDSEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);

}

// Called when the game starts or when spawned
void ATDSEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDSEnemyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ATDSEnemyBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (UTDSStateEffect* effect : StateEffects)
	{
		if (effect)
		{
			WroteSomething |= Channel->ReplicateSubobject(effect, *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)
		}
	}

	return WroteSomething;
}

TArray<class UTDSStateEffect*> ATDSEnemyBase::GetAllCurrentEffects()
{
	return StateEffects;
}

void ATDSEnemyBase::RemoveEffect(UTDSStateEffect* RemoveEffect)
{
	//On Server
	StateEffects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroy)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATDSEnemyBase::AddEffect(UTDSStateEffect* newEffect)
{
	//On Server
	StateEffects.Add(newEffect);

	if (!newEffect->bIsAutoDestroy)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		AddEffectFX_Multicast(newEffect->ParticleEffect, newEffect->BoneName);
	}
}

FVector ATDSEnemyBase::GetAttachLocation()
{
	return FVector();
}

void ATDSEnemyBase::AddEffectFX_Multicast_Implementation(UParticleSystem* Template, FName BoneName)
{
	UTDSTypes::AddEffectFX(Template, this, BoneName, FVector());
}

void ATDSEnemyBase::EffectAdded_OnRep()
{
	SwitchEffect(EffectAdd, true);
}

void ATDSEnemyBase::EffectRemoved_OnRep()
{
	SwitchEffect(EffectRemove, false);
}

void ATDSEnemyBase::SwitchEffect(UTDSStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName HitBoneName = Effect->BoneName;
			USkeletalMeshComponent* skeletal = GetMesh();

			if (skeletal && HitBoneName != NAME_None)
			{
				UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, skeletal, HitBoneName, FVector(),
					FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleComponents.Add(particleComponent);
			}
			else
			{
				UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, GetRootComponent(), HitBoneName, FVector(),
					FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleComponents.Add(particleComponent);
			}
		}
	}
	else
	{
		int removingParticleIndex = ParticleComponents.IndexOfByPredicate([=](UParticleSystemComponent* Component)
			{return Effect && Effect->ParticleEffect && Component && Component->Template && Effect->ParticleEffect == Component->Template; });

		if (ParticleComponents.IsValidIndex(removingParticleIndex))
		{
			ParticleComponents[removingParticleIndex]->DeactivateSystem();
			ParticleComponents[removingParticleIndex]->DestroyComponent();
			ParticleComponents.RemoveAt(removingParticleIndex);
		}

	}
}

void ATDSEnemyBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	
	DOREPLIFETIME(ATDSEnemyBase, StateEffects);
	DOREPLIFETIME(ATDSEnemyBase, EffectAdd);
	DOREPLIFETIME(ATDSEnemyBase, EffectRemove);
}

