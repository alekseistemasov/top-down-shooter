// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSTypes.h"
#include "WeaponBase.generated.h"

class UArrowComponent;
enum class EMovementState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnWeaponReloadAnimStart, UAnimMontage*, Anim, UAnimMontage*, AnimAim, float, ReloadTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadAnimEnd, bool, bIsSuccess);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponFire, UAnimMontage*, AnimFire, UAnimMontage*, AnimFireAim);

UCLASS()
class TDS_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();
	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadAnimStart OnWeaponReloadAnimStart;
	UPROPERTY(BlueprintAssignable)
	FOnWeaponReloadAnimEnd OnWeaponReloadAnimEnd;
	FOnWeaponFire OnWeaponFire;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

	USceneComponent* Root = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
	USkeletalMeshComponent* SkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
	UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
	UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
	UArrowComponent* MagazineDropLocation = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
	UArrowComponent* BulletShellLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;
	UPROPERTY(Replicated)
	FWeaponDynamicInfo WeaponDynamicInfo;

	
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(Server, Reliable)
	void UpdateMovementState_OnServer(EMovementState State);
	void InitWeapon(const FWeaponInfo& WeaponInfo, const FWeaponDynamicInfo& DynamicInfo, int WeaponIndex);
	UFUNCTION(Server, Reliable)
	void SetWeaponFiringState_OnServer(bool bIsFiring);
	void InitReloadAnim();
	void Reload(int AvailableAmmo, int& AmmoTaken);
	void CancelReload();
	int GetWeaponRound();
	FWeaponDynamicInfo GetWeaponDynamicInfo();
	bool IsWeaponReloading();
	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovement_OnServer(FVector NewShootEndLocation, bool bShouldReduceDispersion);

	UPROPERTY()
	FWeaponInfo WeaponSetting;
	
    FVector ShootEndLocation;

	int CurrentWeaponIndex = 0;

private:
	void FireTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DropMagazineTick(float DeltaTime);
	void DropShellTick(float DeltaTime);
	
	void Fire();
	FProjectileInfo GetProjectile();
	int GetNumberProjectileByShot();
	FVector GetFireEndLocation();
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	float GetCurrentDispersion() const;
	void ChangeDispersionByShot();
	void FinishReloadAnim();
	
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(TSubclassOf<AStaticMeshActor> MeshClass, FVector SpawnLocation, FRotator SpawnRotation, float LifeSpan, 
		FVector ImpulseDir, float ImpulseDispersion, float ImpulsePower, float CustomMass);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnShellMesh_Multicast(TSubclassOf<AStaticMeshActor> MeshClass, FVector SpawnLocation, FRotator SpawnRotation, float LifeSpan,
		FVector ImpulseDir, float ImpulseDispersion, float ImpulsePower, float CustomMass);

	UFUNCTION(NetMulticast, Unreliable)
	void PlayWeaponAnim_Multicast(UAnimMontage* Anim, float PlayRate = 1);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitFX_Multicast(UParticleSystem* Template, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* Sound, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponReloadAnimStart_Multicast(UAnimMontage* ReloadAnim, UAnimMontage* ReloadAnimAim, float ReloadTime);

	bool bWeaponIsFiring = false;
	bool bWeaponIsReloading = false;
	bool bIsFireBlock = false;
	bool bDropMagazineFlag = false;
	bool bDropShellFlag = false;
	float FireTimer = 0;
	float ReloadTimer = 0;
	float DropMagazineTimer = 0;
	float DropShellTimer = 0;
	
	
	

	//Dispersion
	bool ShouldReduceDispersion = true;
	float CurrentDispersion = 5.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

};
