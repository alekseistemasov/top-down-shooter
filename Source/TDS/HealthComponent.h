// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Health")
	float CoefDamage = 1.0f;

	UPROPERTY(Replicated)
	float Health = 100.0f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void ReceiveDamage_OnServer(float Damage);
	UFUNCTION(BlueprintCallable, Category = "Health")
	virtual bool TryRestoreHealth(float Amount);

	UFUNCTION(BlueprintCallable, Category = "Health")
	bool GetIsAlive() { return bIsAlive; };

private:
	UFUNCTION(NetMulticast, Reliable)
	void OnHealthChange_Multicast(float NewHealth, float Damage);

	UPROPERTY(Replicated)
	bool bIsAlive = true;

	


	

		
};
