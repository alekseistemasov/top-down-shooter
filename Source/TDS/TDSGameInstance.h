// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDSGameInstance.generated.h"

struct FWeaponInfo;

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& InfoOut);

	UPROPERTY(EditAnywhere)
	UDataTable* WeaponsTable = nullptr;
	
};
