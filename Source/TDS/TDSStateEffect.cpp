// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStateEffect.h"
#include "IGameInteractable.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

bool UTDSStateEffect::InitObject_Implementation(AActor* Actor, FName HitBoneName)
{
	MyActor = Actor;
	BoneName = HitBoneName;

	IIGameInteractable* myInterface = Cast<IIGameInteractable>(MyActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
		return true;
	}
	else
	{
		return false;
	}

	
}

TArray<TEnumAsByte<EPhysicalSurface>> UTDSStateEffect::GetPossibleSurfaces()
{
	return PossibleInteractSurface;
}

bool UTDSStateEffect::GetIsStakable()
{
	return bIsStakable;
}

void UTDSStateEffect::DestroyObject_Implementation()
{
	IIGameInteractable* myInterface = Cast<IIGameInteractable>(MyActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
		
	}

	MyActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDSStateEffect_ExecuteOnce::InitObject_Implementation(AActor* Actor, FName HitBoneName)
{
	bool success = Super::InitObject_Implementation(Actor, HitBoneName);
	if (success)
	    ExecuteOnce();
	
	DestroyObject();
	return success;
}

bool UTDSStateEffect_ExecuteTimer::InitObject_Implementation(AActor* Actor, FName HitBoneName)
{
	bool success = Super::InitObject_Implementation(Actor, HitBoneName);
	if (success)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDSStateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDSStateEffect_ExecuteTimer::Execute, RateTime, true);

		//IIGameInteractable* interactable = Cast<IIGameInteractable>(MyActor);
		//if (ParticleEffect && interactable)
		//{
		//	//FName NameBoneToAttached = interactable->GetAttachBoneName();
		//	//FVector Loc = interactable->GetAttachLocation();
		//	USkeletalMeshComponent* skeletal = Cast<USkeletalMeshComponent>(MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		//	if(skeletal && HitBoneName != NAME_None)
  //              ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, skeletal, HitBoneName, FVector(),
		//			FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		//	else
		//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyActor->GetRootComponent(), HitBoneName, FVector(),
		//			FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		//}

	}
	
	return success;
}

void UTDSStateEffect_ExecuteTimer::DestroyObject_Implementation()
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	/*if (ParticleEmitter)
		ParticleEmitter->DestroyComponent();*/
	Super::DestroyObject_Implementation();
}

void UTDSStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDSStateEffect, BoneName);
	

}
