// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "CharHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldChange, float, ShieldValue);

/**
 * 
 */
UCLASS()
class TDS_API UCharHealthComponent : public UHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldChange OnShieldChange;
	
	void ReceiveDamage_OnServer_Implementation(float Damage) override;
	float GetShieldValue();
	UFUNCTION(BlueprintCallable)
	void SetInvulnerability(bool Invulnerability);
	

	

private:
	UPROPERTY(Replicated)
	float ShieldValue = 100;
	FTimerHandle CoolDownTimer;
	FTimerHandle ShieldRestoreTimer;
	bool bIsInvulnerable = false;
	
	
	UPROPERTY(EditAnywhere, Category = "Health")
	float CoolDownShieldRestoreTime = 5;

	UPROPERTY(EditAnywhere, Category = "Health")
	float ShieldRestoreRate = 0.1;
	
	UPROPERTY(EditAnywhere, Category = "Health")
	float ShieldRestoreValue = 1;

	float ReceiveShieldDamage(float Damage);
	void RestoreShield();
	void CoolDownShieldEnd();

	UFUNCTION(NetMulticast, Reliable)
	void OnShieldChange_Multicast(float NewShieldValue);
	
};
