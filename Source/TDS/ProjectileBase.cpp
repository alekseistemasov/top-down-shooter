// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "CharHealthComponent.h"
#include "Perception/AISense_Damage.h"




// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)
	BulletCollisionSphere->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;

	SetReplicates(true);

}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	if (GetLocalRole() == ROLE_Authority)
	{
		BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileBase::BulletCollisionSphereHit);
	}
	
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileBase::InitProjectile(const FProjectileInfo& InitParam)
{
	//BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	//BulletProjectileMovement->MaxSpeed = 0; //InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	//BulletMesh->SetStaticMesh(InitParam.ProjectileMesh);
	SetVisualMesh_Multicast(InitParam.ProjectileMesh);
	//BulletFX->SetTemplate(InitParam.TrialFx);
	SetTrialFx_Multicast(InitParam.TrialFx);

	ProjectileSetting = InitParam;

	if (!InitParam.ProjectileMesh)
		BulletMesh->DestroyComponent();
	if (!InitParam.TrialFx)
		BulletFX->DestroyComponent();

	InitProjectileVelocity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);
}

FProjectileInfo AProjectileBase::GetProjectileSetting()
{
	return ProjectileSetting;
}

void AProjectileBase::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
		UCharHealthComponent* healthComp = OtherActor->GetComponentByClass<UCharHealthComponent>();
		if (healthComp && healthComp->GetShieldValue() > 0)
		{
			mySurfacetype = EPhysicalSurface::SurfaceType3;
		}

		if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];

			if (myMaterial && OtherComp)
			{
				SpawnHitDecal_Multicast(myMaterial, OtherComp, Hit);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
			
			if (myParticle)
			{
				SpawnHitFX_Multicast(myParticle, Hit);
			}
		}

		if (ProjectileSetting.HitSound)
		{
			SpawnHitSound_Multicast(ProjectileSetting.HitSound, Hit);
		}

		UTDSTypes::AddEffectBySurfaceType(OtherActor, ProjectileSetting.Effect, mySurfacetype, Hit.BoneName);

	}
	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), 0, FVector(), FVector());
	ImpactProjectile();
	
}

void AProjectileBase::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectileBase::SetVisualMesh_Multicast_Implementation(UStaticMesh* Mesh)
{
	BulletMesh->SetStaticMesh(Mesh);
}

void AProjectileBase::SetTrialFx_Multicast_Implementation(UParticleSystem* Template)
{
	BulletFX->SetTemplate(Template);
}

void AProjectileBase::SpawnHitFX_Multicast_Implementation(UParticleSystem* Template, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Template, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AProjectileBase::SpawnHitSound_Multicast_Implementation(USoundBase* Sound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, HitResult.ImpactPoint);
}

void AProjectileBase::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = NewVelocity;
	}
}

void AProjectileBase::ImpactProjectile()
{
	Destroy();
}

void AProjectileBase::InitProjectileVelocity_Multicast_Implementation(float NewInitSpeed, float NewMaxSpeed)
{
	if (BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = GetActorForwardVector() * NewInitSpeed;
		BulletProjectileMovement->MaxSpeed = NewMaxSpeed;
	}
}

