// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IGameInteractable.h"
#include "TDSEnemyBase.generated.h"

UCLASS()
class TDS_API ATDSEnemyBase : public ACharacter, public IIGameInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSEnemyBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	//interface
	TArray<class UTDSStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDSStateEffect* RemoveEffect) override;
	void AddEffect(UTDSStateEffect* newEffect) override;
	FVector GetAttachLocation() override;


private:
	UPROPERTY(Replicated)
	TArray<class UTDSStateEffect*> StateEffects;
	UPROPERTY(ReplicatedUsing = EffectAdded_OnRep)
	UTDSStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemoved_OnRep)
	UTDSStateEffect* EffectRemove = nullptr;

	UFUNCTION(NetMulticast, Reliable)
	void AddEffectFX_Multicast(UParticleSystem* Template, FName BoneName);

	TArray<UParticleSystemComponent*> ParticleComponents;

	UFUNCTION()
	void EffectAdded_OnRep();
	UFUNCTION()
	void EffectRemoved_OnRep();

	void SwitchEffect(UTDSStateEffect* Effect, bool bIsAdd);

	

};
