// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::ReceiveDamage_OnServer_Implementation(float Damage)
{
	if (!bIsAlive) return;
	
	Damage *= CoefDamage;

	Health -= Damage;
	OnHealthChange_Multicast(Health, Damage);

	if (Health <= 0.0f)
	{
		bIsAlive = false;
	    OnDead.Broadcast();
	}
	

	
}

bool UHealthComponent::TryRestoreHealth(float Amount)
{
	//On Server
	if (Health >= 100)
		return false;
	
	Health += Amount;
	if (Health > 100)
		Health = 100;

	
	OnHealthChange_Multicast(Health, 0);
	return true;
}



void UHealthComponent::OnHealthChange_Multicast_Implementation(float NewHealth, float Damage)
{
	OnHealthChange.Broadcast(NewHealth, Damage);

}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
	DOREPLIFETIME(UHealthComponent, bIsAlive);

}

