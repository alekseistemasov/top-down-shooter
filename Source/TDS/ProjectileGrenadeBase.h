// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "ProjectileGrenadeBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileGrenadeBase : public AProjectileBase
{
	GENERATED_BODY()

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void InitProjectile(const FProjectileInfo& InitParam) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void ImpactProjectile() override;
	void GrenadeTick(float DeltaTime);
	void InitExplode();
	UFUNCTION(NetMulticast, Reliable)
	void SpawnFXAtLocation_Multicast(UParticleSystem* Template, FVector Location);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnSoundAtLocation_Multicast(USoundBase* Sound, FVector Location);

    float ExplodeTimer = 0;
	UPROPERTY(EditDefaultsOnly)
	bool bExplodeTimerActive = true;
	
	
};
