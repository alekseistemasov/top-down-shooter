// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileGrenadeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/DamageEvents.h"

int DebugExplodeShow = 1;
FAutoConsoleVariableRef CVarExplodeShow(TEXT("ExplodeShow"), DebugExplodeShow, TEXT("DebugExplode"));

void AProjectileGrenadeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GetLocalRole() == ROLE_Authority)
	{
		GrenadeTick(DeltaTime);
	}
}

void AProjectileGrenadeBase::InitProjectile(const FProjectileInfo& InitParam)
{
	Super::InitProjectile(InitParam);
	ExplodeTimer = InitParam.TimeToExplode;
	
}

void AProjectileGrenadeBase::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileGrenadeBase::ImpactProjectile()
{
	if (!bExplodeTimerActive)
		InitExplode();

	
}

void AProjectileGrenadeBase::GrenadeTick(float DeltaTime)
{
	// On Server
	if (bExplodeTimerActive) {
		if (ExplodeTimer <= 0)
			InitExplode();
		else
			ExplodeTimer -= DeltaTime;
	}
}

void AProjectileGrenadeBase::InitExplode()
{
	//On Server
	bExplodeTimerActive = false;

	if (DebugExplodeShow) {
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileInnerRadius, 12, FColor::Red, false, 3);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileOuterRadius, 12, FColor::Green, false, 3);
	}

	if (ProjectileSetting.ExploseFX)
		SpawnFXAtLocation_Multicast(ProjectileSetting.ExploseFX, GetActorLocation());
	if (ProjectileSetting.ExploseSound)
		SpawnSoundAtLocation_Multicast(ProjectileSetting.ExploseSound, GetActorLocation());

	TArray<AActor*> IgnoredActors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExploseMaxDamage, 
		ProjectileSetting.ExploseMaxDamage * 0.5, GetActorLocation(), ProjectileSetting.ProjectileInnerRadius, 
		ProjectileSetting.ProjectileOuterRadius, ProjectileSetting.DamageFalloff, FRadialDamageEvent().DamageTypeClass, IgnoredActors, this, nullptr);
	
	Destroy();
}

void AProjectileGrenadeBase::SpawnSoundAtLocation_Multicast_Implementation(USoundBase* Sound, FVector Location)
{
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, Location);
}

void AProjectileGrenadeBase::SpawnFXAtLocation_Multicast_Implementation(UParticleSystem* Template, FVector Location)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Template, Location);
}
