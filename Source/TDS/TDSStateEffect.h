// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDSStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDSStateEffect : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    bool InitObject(AActor* Actor, FName HitBoneName);
	TArray<TEnumAsByte<EPhysicalSurface>> GetPossibleSurfaces();
	bool GetIsStakable();

	bool IsSupportedForNetworking() const override { return true; };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	UParticleSystem* ParticleEffect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsAutoDestroy = false;

	UPROPERTY(Replicated)
	FName BoneName;
	

protected:
	UFUNCTION(BlueprintNativeEvent)
	void DestroyObject();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;
	UPROPERTY(BlueprintReadOnly)
	AActor* MyActor = nullptr;
};



UCLASS()
class TDS_API UTDSStateEffect_ExecuteOnce : public UTDSStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject_Implementation(AActor* Actor, FName HitBoneName) override;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Event")
	void ExecuteOnce();

	
};

UCLASS()
class TDS_API UTDSStateEffect_ExecuteTimer : public UTDSStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject_Implementation(AActor* Actor, FName HitBoneName) override;

	

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Event")
	void Execute();

	virtual void DestroyObject_Implementation() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;
	

	UParticleSystemComponent* ParticleEmitter = nullptr;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};
	

	
