// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"
#include "TDSTypes.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& InfoOut)
{
	FWeaponInfo* info = nullptr;
	if (WeaponsTable) {
		info = WeaponsTable->FindRow<FWeaponInfo>(WeaponName, "");
		if (info) {
			InfoOut = *info;
			return true;
		}
		else
			return false;
	}
	return false;
}
