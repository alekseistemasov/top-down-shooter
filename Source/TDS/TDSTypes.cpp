// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSTypes.h"
#include "TDSStateEffect.h"
#include "IGameInteractable.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

void UTDSTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDSStateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName HitBoneName)
{
	if (!TakeEffectActor || !AddEffectClass) return;

	UTDSStateEffect* effect = Cast<UTDSStateEffect>(AddEffectClass->GetDefaultObject());
	IIGameInteractable* interactable = Cast<IIGameInteractable>(TakeEffectActor);

	if (!effect || !interactable) return;
	
	bool bCanAddEffect = false;
	if (effect->GetPossibleSurfaces().Find(SurfaceType) != INDEX_NONE)
	{
		if (!effect->GetIsStakable())
		{
			
			if (!interactable->GetAllCurrentEffects().FindByPredicate([=](UTDSStateEffect* stateEffect) 
				{return stateEffect->GetClass() == AddEffectClass; }))
			{
				bCanAddEffect = true;
			}
			
		}
		else
		{
			bCanAddEffect = true;
		}
	}
	
	if (bCanAddEffect)
	{
		UTDSStateEffect* newEffect = NewObject<UTDSStateEffect>(TakeEffectActor, AddEffectClass); 
		if (newEffect)
		{
			newEffect->InitObject(TakeEffectActor, HitBoneName);
			
		}
	}
}

void UTDSTypes::AddEffectFX(UParticleSystem* Template, AActor* Actor, FName HitBoneName, FVector Offset)
{
	if (!Template || !Actor) return;

	ACharacter* character = Cast<ACharacter>(Actor);
	USkeletalMeshComponent* skeletal = nullptr;
	if (character)
	{
		skeletal = character->GetMesh();
	}


	if (skeletal && HitBoneName != NAME_None)
	{
		UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Template, skeletal, HitBoneName, Offset,
			FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		
	}
	else
	{
		UParticleSystemComponent* particleComponent = UGameplayStatics::SpawnEmitterAttached(Template, Actor->GetRootComponent(), HitBoneName, Offset,
			FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		
	}
}
