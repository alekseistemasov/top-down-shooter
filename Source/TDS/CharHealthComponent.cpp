// Fill out your copyright notice in the Description page of Project Settings.


#include "CharHealthComponent.h"
#include "Net/UnrealNetwork.h"

void UCharHealthComponent::ReceiveDamage_OnServer_Implementation(float Damage)
{
	if (!GetIsAlive() || bIsInvulnerable) return;
	
	Damage *= CoefDamage;

	float remainingDamage = ReceiveShieldDamage(Damage);
	Super::ReceiveDamage_OnServer_Implementation(remainingDamage);
	if (GetWorld())
	{
	    if (Health <= 0)
		    GetWorld()->GetTimerManager().ClearTimer(CoolDownTimer);
	}

}

float UCharHealthComponent::GetShieldValue()
{
	return ShieldValue;
}

void UCharHealthComponent::SetInvulnerability(bool Invulnerability)
{
	bIsInvulnerable = Invulnerability;
}

float UCharHealthComponent::ReceiveShieldDamage(float Damage)
{
	float remainingDamage = Damage - ShieldValue;
	if (remainingDamage < 0)
		remainingDamage = 0;
	
	ShieldValue -= Damage;

	if (ShieldValue < 0.0f)
	{
	    ShieldValue = 0.0f;
		//FX
		//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
	}
	

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(CoolDownTimer, this, &UCharHealthComponent::CoolDownShieldEnd, CoolDownShieldRestoreTime, false);

		GetWorld()->GetTimerManager().ClearTimer(ShieldRestoreTimer);
	}

	OnShieldChange_Multicast(ShieldValue);
	return remainingDamage;
}

void UCharHealthComponent::RestoreShield()
{
	ShieldValue += ShieldRestoreValue;
	if (ShieldValue > 100.0f)
	{
		ShieldValue = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(ShieldRestoreTimer);
		}
	}
	OnShieldChange_Multicast(ShieldValue);
}

void UCharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldRestoreTimer, this, &UCharHealthComponent::RestoreShield, ShieldRestoreRate, true);
	}
}

void UCharHealthComponent::OnShieldChange_Multicast_Implementation(float NewShieldValue)
{
	OnShieldChange.Broadcast(NewShieldValue);
}

void UCharHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCharHealthComponent, ShieldValue);

}
